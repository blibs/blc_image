#include "blc_image.h" //This include blc_core.h

int main(int argc, char **argv){
    blc_array input_image, float_image, uchar_output_image, downscale_image;
    int width, height;
    char const *input_filename="lenaRGB32.blc";
    char const *float_output_filename="t_convert_image/float_result.tsv";
    char const *uchar_output_filename="t_convert_image/uchar_result.tsv";

   // fprintf(stderr, "\nLoad the png file passed as argument ( '%s' by default ).\nAdd a red cross in the middle and save the result in '%s'.\n", input_filename, output_filename);
    input_image.init_with_blc_file(input_filename);
    
    //Depending on the format, the .dims[0].length is the width ('Y800') or the bytes_per_pixel( i.e. 3 for 'RGB3')
    //In order to make it works for any format, we compare with the number of dims.
    width=input_image.dims[input_image.dims_nb-2].length;
    height=input_image.dims[input_image.dims_nb-1].length;
    
    //We initiate the output image that we want after conversion. (The conversion are not all possible yet).
    blc_image_init(&float_image, 'FL32', 'Y800', width, height);
    blc_image_init(&uchar_output_image, 'UIN8', 'Y800', width, height);
    blc_image_init(&downscale_image, 'FL32', 'Y800', width/2, height/2);

    //We convert uchar rgb3 -> float Y800
    blc_image_convert(&float_image, &input_image);
    
    //We convert float Y800 -> uchar Y800
    blc_image_convert(&uchar_output_image, &float_image);
        
    //We divide the size of the image by 2
    blc_image_downscale(&downscale_image, &float_image);
    
    //We save the result of the conversion in a human readable format
    downscale_image.save_tsv_file(float_output_filename);
    uchar_output_image.save_tsv_file(uchar_output_filename);
    
    return 0;
}
