
BLC image
=========

- Copyright : [ETIS](http://www.etis.ensea.fr/neurocyber) - ENSEA, University of Cergy-Pontoise, - CNRS (2011-2016)  
- Author    : [Arnaud Blanchard](http://arnaudblanchard.thoughtsheet.com)  
- Licence   : [CeCILL v2.1](http://www.cecill.info/licences/Licence_CeCILL_V2-en.html)  

Manipulate images as **blc_array**. It will allow you to read/write image files or  convert them. For more complex functionalities, consider using [ImageMagick](https://www.imagemagick.org).

[Standard blaar install](https://framagit.org/blaar/blaar/blob/master/INSTALL) automatically installs it. However, you can [manually install the library](INSTALL)  


[More details and examples](https://framagit.org/blaar/blc_image/wikis/home)

