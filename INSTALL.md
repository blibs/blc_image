INSTALL
=======

You are advised to used [standard blaar install](https://framagit.org/blaar/blaar/wikis/INSTALL) but you are free to make a manual installation

Manual install with cmake
-------------------------
You need to install [**blc_core**](https://framagit.org/blaar/blc_core) first. The libraries **jpeglib** and **pnglib** are advised for more functionalities.

- Ubuntu: 

```sh
sudo apt-get install libpng12-dev libjpeg-dev 
```
- OSX with [homebrew](http://brew.sh) and any C++ compiler

```sh
brew install libpng libjpeg
```
You can copy past the following code:

```sh
git clone https://framagit.org/blaar/blc_image.git
cd blc_image
mkdir build
cd build
cmake ..
make
sudo make install
```
The created library `libblc_image.{so|dylib}` and `libblc_image.a` will be in `/usr/local/lib`.  Includes in `/usr/local/include/blc_image` and a cmake config file in `/usr/local/share/blc_image`
 
You can create a debian package (.deb) with:

    make package

Manual install with simple makefiles
====================================

All the sources are in src, includes in include and the previous lines allow you to know where to put the result of your build.
