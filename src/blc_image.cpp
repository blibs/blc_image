/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 
 *
 *
 *  Created on: August 1, 2015
 *      Author: Arnaud Blanchard
 */

#include "blc_image.h"
#include "blc_tools.h"
#include "jpeg_tools.h"

typedef struct {
    uchar r, g, b;
} type_RGB;

static type_RGB *RGB_from_YUYV = NULL;

static void* init_RGB_from_YUYV()
{
    int Y, Cb, Cr;
    int i, j;
    float G_tmp;
    static uchar R[256], B;
    
    if (RGB_from_YUYV == NULL)
    {
        RGB_from_YUYV = MANY_ALLOCATIONS(256*256*256, type_RGB);
        FOR_INV(Y, 256)
        {
            FOR_INV(j,256)
            R[j] = CLIP_UCHAR(Y+1.13983*(j-128)); //It does not depend on Cb
            FOR_INV(Cb, 256)
            {
                B = CLIP_UCHAR(Y+2.03211*(Cb-128)); // It does not depend on Cr
                G_tmp = -0.58060 * (Cb - 128);
                
                FOR_INV(Cr, 256)
                {
                    i = Y + (Cb << 8) + (Cr << 16);
                    
                    // Version Wikipedia
                    RGB_from_YUYV[i].r = R[Cr];
                    RGB_from_YUYV[i].b = (uchar) CLIP_UCHAR(Y-0.39465*(Cr-128) + G_tmp);
                    RGB_from_YUYV[i].b = B;
                }
            }
        }
    }
    return NULL;
}

void blc_YUYV_to_RGB3(blc_array *dest, blc_array const *src)
{
    int i, j;
    uchar *data = (uchar*)src->data;
    type_RGB *dest_data;
    int Y, Cb = 128, Cr = 128;
    
    init_RGB_from_YUYV();
    dest_data = (type_RGB*) dest->data;
    if (dest->size % 2) EXIT_ON_ERROR("The output size must be multiple of two but it is %ld ", dest->size);
    for (i = 0; i != (int) dest->size; dest_data += 2)
    {
        Y = data[i++];
        Cb = data[i++];
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[0] = RGB_from_YUYV[j];
        
        Y = data[i++];
        Cr = data[i++];
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[1] = RGB_from_YUYV[j];
    }
}

void blc_yuv2_to_RGB3(blc_array *dest, blc_array const *src)
{
    int i, j;
    uchar *data = (uchar*) src->data;
    int Y, Cb = 128, Cr = 128;
    type_RGB *dest_data = (type_RGB*) dest->data;
    
    init_RGB_from_YUYV();
    if (dest->size % 2) EXIT_ON_ERROR("The output size must be multiple of two but it is %ld ", dest->size);
    for (i = 0; i != (int) dest->size; dest_data += 2)
    {
        Cb = data[i++];
        Y = data[i++];
        
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[0] = RGB_from_YUYV[j];
        
        Cr = data[i++];
        Y = data[i++];
        
        j = Y + (Cb << 8) + (Cr << 16);
        dest_data[1] = RGB_from_YUYV[j];
    }
}

/*
 R G R G R G
 G B G B G B
 R G R G
 
 1 2 1  A B C
 2 4 2  D E F
 1 2 1  G H I
 */

void blc_Y800_from_BA81(blc_array *dest, blc_array const *src)
{
    uchar A, B, C, D, E, F, G, H, I;
    int sum, ADG, BEH, CFI;
    int i, j;
    int src_width = src->dims[0].length;
    int dest_width = dest->dims[0].length;
    //  int dest_height = dest->dims[1].length;
    
    A = src->uchars[0];
    B = src->uchars[1];
    C = src->uchars[2];
    D = src->uchars[src_width];
    E = src->uchars[1 + src_width];
    F = src->uchars[2 + src_width];
    G = src->uchars[src_width * 2];
    H = src->uchars[1 + 2 * src_width];
    I = src->uchars[2 + 2 * src_width];
    ADG = A + 2 * D + G;
    BEH = B + 2 * E + H;
    CFI = C + 2 * F + I;
    
    sum = ADG + 2 * BEH + CFI;
    
    for (j = 2; j < dest_width; j++)
    {
        A = src->uchars[(j - 2) * src_width];
        B = src->uchars[1 + (j - 2) * src_width + 1];
        D = src->uchars[(j - 1) * src_width];
        E = src->uchars[1 + (j - 1) * src_width];
        G = src->uchars[j * src_width];
        H = src->uchars[1 + j * src_width];
        ADG = A + 2 * D + G;
        BEH = B + 2 * E + H;
        CFI = C + 2 * F + I;
        sum = ADG + 2 * BEH;
        for (i = 2; i < dest_width; i++)
        {
            C = src->uchars[2 + (j - 2) * src_width];
            F = src->uchars[2 + (j - 1) * src_width];
            I = src->uchars[2 + j * src_width];
            CFI = C + 2 * F + I;
            sum = ADG + 2 * BEH + CFI;
            dest->uchars[i - 1 + (j - 1) * dest_width] = sum / 4;
            ADG = BEH;
            BEH = CFI;
        }
    }
}

void blc_Y800_float_from_BA81(blc_array *dest, blc_array const *src)
{
    int i, j;
    int src_width = src->dims[0].length;
    int dest_width = dest->dims[0].length;
    int dest_height = dest->dims[1].length;
    
    FOR_INV(j, dest_height)
    {
        FOR_INV(i, dest_width)
        {
            dest->floats[i + j * dest_width] = (src->uchars[i * 2 + j * 2 * src_width] + src->uchars[i * 2 + 1 + j * 2 * src_width] + src->uchars[i * 2 + (j * 2 + 1) * src_width] + src->uchars[i * 2 + 1 + (j * 2 + 1) * src_width] + 2.f) / 1024.f;
        }
    }
}

int blc_image_get_bytes_per_pixel(blc_array const *image)
{
    switch (image->format)
    {
        case 'Y800':
        case 'BA81':
            return 1;
            break;
        case 'yuv2':
        case 'YUYV':
        case 'UYVY':
            return 2;
            break;
        case 'RGB3':
            return 3;
            break;
        case 'RGBA':
            return 4;
            break;
        case 'MJPG':
        case 'JPEG':
        case 'PNG':
            return -1;
            break;
        default:
            EXIT_ON_ARRAY_ERROR(image, "Unknonw pixel format");
    }
    return 0;
}

void blc_image_def(blc_array *array, uint32_t type, uint32_t format, size_t width, size_t height)
{
    int bytes_per_pixel;
    
    array->type = type;
    array->format = format;
    bytes_per_pixel = blc_image_get_bytes_per_pixel(array);
    switch (bytes_per_pixel)
    {
            case -1:
               array->add_dim(width*height*3); //We take the max we can imagine
            break;
            case 1:
               array->add_dim(width);
               array->add_dim(height);
            break;
        default:
            array->add_dim(bytes_per_pixel);
            array->add_dim(width);
            array->add_dim(height);
    }
}

void blc_image_init(blc_array *array, uint32_t type, uint32_t format, size_t width, size_t height)
{
    blc_image_def(array, type, format, width, height);
    array->allocate();
}

/*
 blc_image_info::blc_image_info(uint32_t format, int width, int height):format(format), width(width), height(height)
 {
 bytes_per_pixel = blc_get_bytes_per_pixel(format);
 pixels_nb=width*height;
 }*/

void blc_image_convert(blc_array *output, blc_array const *input)
{
    int i;
    uint32_t output_format_str, input_format_str, input_type_str, output_type_str;
    
    if (input->format == output->format)
    {
        if (input->type == output->type) memcpy(output->uchars, input->uchars, input->size);
        else if (input->type == 'UIN8' && output->type == 'FL32') FOR_INV(i, output->size/sizeof(float))
            output->floats[i] = (input->uchars[i] + 0.5f) / 256;
        else if (input->type == 'FL32' && output->type == 'UIN8') FOR_INV(i, output->size)
            output->uchars[i] = CLIP_UCHAR(input->floats[i]*256-0.5f);
        else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
    }
    else
    {
        switch (input->format)
        {
            case 'BA81':
                switch (output->format)
            {
                case 'Y800':
                    if (input->type == output->type) blc_Y800_from_BA81(output, input);
                    else if (input->type == 'UIN8' && output->type == 'FL32') blc_Y800_float_from_BA81(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
                
            case 'yuv2':
                switch (output->format)
            {
                case 'Y800':
                    if (input->type == output->type) FOR_INV(i, output->size)
                        output->uchars[i] = input->uchars[i * 2 + 1];
                    else if (input->type == 'UIN8' && output->type == 'FL32') FOR_INV(i, output->size/sizeof(float))
                        output->floats[i] = (input->uchars[i * 2 + 1] + 0.5f) / 256;
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                case 'RGB3':
                    if (input->type == output->type) blc_yuv2_to_RGB3(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
            case 'YUYV':
                switch (output->format)
            {
                case 'Y800':
                    if (input->type == output->type) FOR_INV(i, output->size)
                        output->uchars[i] = input->uchars[i * 2];
                    else if (input->type == 'UIN8' && output->type == 'FL32') FOR_INV(i, output->size/sizeof(float))
                        output->floats[i] = (input->uchars[i * 2] + 0.5f) / 256;
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    
                    break;
                case 'RGB3':
                    if (input->type == output->type) blc_YUYV_to_RGB3(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
            case 'UYVY':
                switch (output->format)
            {
                case 'Y800':
                    if (input->type == output->type) FOR_INV(i, output->size)
                        output->uchars[i] = input->uchars[i * 2 + 1];
                    else if (input->type == 'UIN8' && output->type == 'FL32') FOR_INV(i, output->size/sizeof(float))
                        output->floats[i] = (input->uchars[i * 2 + 1] + 0.5f) / 256;
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    
                    break;
                case 'RGB3':
                    if (input->type == output->type) blc_YUYV_to_RGB3(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
            case 'Y800':
                switch (output->format)
            {
                case 'JPEG':
                    if (input->type == output->type) blc_image_to_jpeg(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
            case 'RGB3':
                switch (output->format)
            {
                case 'Y800':
                    if (input->type == output->type)
                        FOR_INV(i, output->size) output->uchars[i] = (input->uchars[i * 3]+input->uchars[i * 3+1]+input->uchars[i * 3+2])/3;
                    else if ((input->type=='UIN8') && (output->type=='FL32'))
                        FOR_INV(i, output->size/sizeof(float)) output->floats[i] = (input->uchars[i * 3]+input->uchars[i * 3+1]+input->uchars[i * 3+2]+0.5f)/(3*256.f);
                    else if ((input->type=='FL32') && (output->type=='UIN8'))
                        FOR_INV(i, output->size/sizeof(float)) output->uchars[i] = (input->floats[i * 3]+input->floats[i * 3+1]+input->floats[i * 3+2])*256/3.f-0.5f;
                    else
                        EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                case 'JPEG':
                    if (input->type == output->type) blc_jpeg_to_image(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    break;
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
                case 'JPEG':case 'MJPG':
                switch (output->format)
            {
                    case 'Y800':
                    if (input->type == output->type) blc_jpeg_to_image(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    
                    break;
                    case 'RGB3':
                    if (input->type == output->type) blc_jpeg_to_image(output, input);
                    else EXIT_ON_ERROR("Conversion from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                    
                    break;
                    
                default:
                    EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
            }
                break;
            default:
                EXIT_ON_ERROR("Input format '%.4s' is not managed", UINT32_TO_STRING(input_format_str, input->format));
        }
    }
}

/*The output image must exactly be 2 times smaller than the input */
void blc_image_downscale(blc_array *output, blc_array const *input)
{
    size_t i, j, width, height;
    uint32_t output_format_str, input_format_str, input_type_str, output_type_str;
    
    switch (input->format)
    {
        case 'Y800':
            if ((input->type == output->type) &&  (input->type=='UIN8')){
                width = output->dims[0].length;
                height = output->dims[1].length;
                if (width * 2 != input->dims[0].length) EXIT_ON_ARRAY_ERROR(output, "Output width is not half input width '%d'", output->dims[0].length);
                if (height * 2 != input->dims[1].length) EXIT_ON_ARRAY_ERROR(output, "Output height is not half input height '%d'", output->dims[1].length);
                FOR_INV(j, height)
                FOR_INV(i, width)
                output->uchars[i + j * width] = (input->uchars[i * 2+(j * 2) * width] + input->uchars[i * 2+(j * 2) * width+1] + input->uchars[i * 2+(j * 2+1) * width] + input->uchars[i * 2+(j * 2+1) * width + 1])/4;
            }
            else if ((input->type == output->type) &&  (input->type=='FL32')){
                width = output->dims[0].length;
                height = output->dims[1].length;
                if (width * 2 != input->dims[0].length) EXIT_ON_ARRAY_ERROR(output, "Output width is not half input width '%d'", output->dims[0].length);
                if (height * 2 != input->dims[1].length) EXIT_ON_ARRAY_ERROR(output, "Output height is not half input height '%d'", output->dims[1].length);
                FOR_INV(j, height)
                FOR_INV(i, width)
                output->floats[i + j * width] = (input->floats[i * 2+(j * 2) * width] + input->floats[i * 2+(j * 2) * width+1] + input->floats[i * 2+(j * 2+1) * width] + input->floats[i * 2+(j * 2+1) * width + 1])/4.f;
            }
            
            else EXIT_ON_ERROR("Downscale from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
            
            break;
        case 'yuv2':
            switch (output->format)
        {
            case 'Y800':
                if ((input->type == output->type) &&  (input->type=='UIN8'))
                {
                    width = output->dims[0].length;
                    height = output->dims[1].length;
                    if (width * 2 != input->dims[1].length) EXIT_ON_ARRAY_ERROR(output, "Output width is not half input width '%d'", output->dims[1].length);
                    if (height * 2 != input->dims[2].length) EXIT_ON_ARRAY_ERROR(output, "Output height is not half input height '%d'", output->dims[2].length);
                    FOR_INV(j, height)
                    FOR_INV(i, width)
                    output->uchars[i + j * width] = (input->uchars[i * 4 + 1 + (j * 2) * width * 4] + input->uchars[i * 4 + 3 + (j * 2) * width * 4] + input->uchars[i * 4 + 1 + (j * 2 + 1) * 4 * width] + input->uchars[i * 4 + 3 + (j * 2 + 1) * 4 * width])
                    / 4;
                }
                else EXIT_ON_ERROR("Downscale from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                break;
            default:
                EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
        }
            break;
        case 'UYVY':
            switch (output->format)
        {
            case 'Y800':
                if (input->type == output->type)
                {
                    width = output->dims[0].length;
                    height = output->dims[1].length;
                    if (width * 2 != input->dims[1].length) EXIT_ON_ARRAY_ERROR(output, "Output width is not half input width '%d'", output->dims[1].length);
                    if (height * 2 != input->dims[2].length) EXIT_ON_ARRAY_ERROR(output, "Output height is not half input height '%d'", output->dims[2].length);
                    FOR_INV(j, height)
                    FOR_INV(i, width)
                    output->uchars[i + j * width] = (input->uchars[i * 4 + 1 + (j * 2) * width * 4] + input->uchars[i * 4 + 3 + (j * 2) * width * 4] + input->uchars[i * 4 + 1 + (j * 2 + 1) * 4 * width] + input->uchars[i * 4 + 3 + (j * 2 + 1) * 4 * width])
                    / 4;
                }
                else EXIT_ON_ERROR("Downscale from type %.4s to type %.4s is not managed for conversion from format '%.4s' to format '%.4s'.", UINT32_TO_STRING(input_type_str, input->type), UINT32_TO_STRING(output_type_str, output->type), UINT32_TO_STRING(input_format_str, input->format), UINT32_TO_STRING(output_format_str, output->format));
                break;
            default:
                EXIT_ON_ERROR("Output format '%.4s' is not managed with input format '%.4s'.", UINT32_TO_STRING(output_format_str, output->format), UINT32_TO_STRING(input_format_str, input->format));
        }
            break;
        default:
            EXIT_ON_ARRAY_ERROR(input, "Input format is not managed ");
    }
}

