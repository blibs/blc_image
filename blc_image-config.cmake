find_package(blc_core REQUIRED)

find_path(BLC_IMAGE_INCLUDE_DIR blc_image.h PATH_SUFFIXES blc_image)
find_library(BLC_IMAGE_LIBRARY blc_image )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blc_image DEFAULT_MSG  BLC_IMAGE_LIBRARY BLC_IMAGE_INCLUDE_DIR)

mark_as_advanced(BLC_IMAGE_INCLUDE_DIR BLC_IMAGE_LIBRARY )

set(BL_INCLUDE_DIRS ${BLC_IMAGE_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BLC_IMAGE_LIBRARY} ${BL_LIBRARIES} )
