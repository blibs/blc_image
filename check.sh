# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2016)
# Author: Arnaud Blanchard (November 2016)
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.


set -o errexit  #Exit on first error
set -o nounset  #Error if a variable is used but nont defined
set -o pipefail #Error if a pipe fail

cd $(dirname $0)
project=$(basename $PWD)
blaar_dir=$PWD/../..

echo
echo "Checking blc_image"
echo "=================="
echo

LOG=$(mktemp) || exit 1
trap 'rm -f "$LOG"' EXIT INT HUP TERM

$blaar_dir/compile.sh t_convert_image 2>&1 > $LOG || { cat $LOG; echo "ERROR: Fail compiling t_load_and_save_png"; exit 1; } #  2>&1 :  stderr is redirected like stdout
$blaar_dir/bin/t_convert_image  2>&1 > $LOG || {  cat $LOG; echo "ERROR: Fail executing bin/t_load_and_save_png"; exit 2; }
git diff --exit-code t_convert_image/float_result.tsv  2>&1 > $LOG  || { cat $LOG;  echo "ERROR: The result of $test/float_result.tsv is not the one expected"; exit 3; }
git diff --exit-code t_convert_image/uchar_result.tsv  2>&1 > $LOG  || { cat $LOG;  echo "ERROR: The result of $test/uchar_result.tsv is not the one expected"; exit 3; }
echo "- t_convert_image: compilation:OK, execution:OK, results:OK"
echo

exit 0
