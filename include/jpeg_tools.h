
#ifndef JPEG_TOOLS_H
#define JPEG_TOOLS_H

#include "blc_array.h"

/**
 \defgroup jpeg jpeg functions
 \{
 Functions for jpeg converstions. Need libjpeg
 */

START_EXTERN_C

///Allocate the blc_array to received the jpeg data. The data is not updated.
void blc_image_jpeg_init(blc_array *image, blc_mem const *mem);
///Convert jpeg data to an image of blc_array
void blc_jpeg_to_image(blc_array *dest, blc_mem const *mem);
///Convert image to jpeg data
void blc_image_to_jpeg(blc_mem *mem, blc_array const *src);

END_EXTERN_C
///\}
#endif
