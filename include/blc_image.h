/*
 Basic Library for C/C++ (blclib)
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2015)
 
 Author:  Arnaud Blanchard
 
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
  In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
  that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
  and, more generally, to use and operate it in the same conditions as regards security.
  The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. *
 */

/**
\mainpage Summary
 
Specific functions to manipulate blc_array as images:
- \ref blc_image generic image functions and simple conversions ( Y800, YUYV, yuv2, RGB3, RGBA, BA81, ...)
- \ref jpeg  functions for jpeg images (need libjpeg)
- \ref png functions for png formats (need libpng)
 
*/

#ifndef BLC_IMAGE_H
#define BLC_IMAGE_H

#include "blc_core.h"
#include "stdint.h" //uint32_t

/**
 @defgroup blc_image blc_image functions
 @{
 */

START_EXTERN_C

/**Return the number of bytes for a pixel of the format. -1 is returned if this data is undefined (i.e. JPEG)*/
int blc_image_get_bytes_per_pixel(blc_array const *image);

///Define an blc_array for containing the image. Do not allocate the memory
void blc_image_def(blc_array *array, uint32_t type, uint32_t format, size_t width, size_t height);

///Same as blc_image_def but allocates the memory;
void blc_image_init(blc_array *array, uint32_t type, uint32_t format, size_t width, size_t height);

/// Copy image and make the conversion if needed. Be carefull, few checks are done if the memory is not big enough
void blc_image_convert(blc_array *output, blc_array const *input);

/// Copy and downscale by 2 the image. Make the conversion if needed. Be carefull, few checks are done if the memory is not big enough
void blc_image_downscale(blc_array *output, blc_array const *input);

END_EXTERN_C
///@}

#endif
